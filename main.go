package main

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/joho/godotenv"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"bytes"
	"compress/zlib"
	"io/ioutil"
	"encoding/binary"
	"time"
	"os/exec"
)

type RequestObj struct {
	uri            string
	request_method string
	data           string
	bearer         string
	user_agent     string
}

func Request(request RequestObj) io.ReadCloser {

	client := &http.Client{}
	req, err := http.NewRequest(request.request_method, request.uri, strings.NewReader(request.data))

	if request.bearer != "" {
		req.Header.Add("Authorization", request.bearer)
	}

	if request.data != "" {
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	}

	if request.user_agent != "" {
		req.Header.Add("User-Agent", request.user_agent)
	}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}

	return resp.Body

}

type OauthResponse struct {
	AccessToken string `json:"access_token"`
}

type Item struct {
	AppName       string   `json:"appName"`
	CatalogItemId string   `json:"catalogItemId"`
	Info          ItemInfo `json:"items"`
}

type ItemInfo struct {
	Manifest ItemManifest `json:"MANIFEST"`
}

type ItemManifest struct {
	Signature    string `json:"signature"`
	Distribution string `json:"distribution"`
	Path         string `json:"path"`
}

type ItemArray struct {
	Collection []Item
}

type ChunkManifest struct {
	AppNameString string            `json:"AppNameString"`
	ChunkHashList map[string]string `json:"ChunkHashList"`
	DataGroupList map[string]string `json:"DataGroupList"`
	FileManifestList []FileManifest `json:"FileManifestList"`
}

type Chunk struct {
	Guid string `json:"Guid"`
	Offset string `json:"Offset"`
	Size string `json:"Size"`
}

type FileManifest struct {
	Filename string `json:"Filename"`
	FileChunkParts []Chunk `json:"FileChunkParts"`
}

type FileDownloader struct {

}

func oauthLogin() string {
	uri := "https://account-public-service-prod03.ol.epicgames.com/account/api/oauth/token"
	username := os.Getenv("username")
	password := os.Getenv("password")
	form := url.Values{}
	form.Add("grant_type", "password")
	form.Add("username", username)
	form.Add("password", password)
	form.Add("includePerms", "true")

	req := RequestObj{
		uri:            uri,
		request_method: "POST",
		data:           form.Encode(),
		bearer:         "basic MzRhMDJjZjhmNDQxNGUyOWIxNTkyMTg3NmRhMzZmOWE6ZGFhZmJjY2M3Mzc3NDUwMzlkZmZlNTNkOTRmYzc2Y2Y=",
	}
	resp := Request(req)

	var result OauthResponse
	json.NewDecoder(resp).Decode(&result)
	access_token := result.AccessToken

	//log.Println(access_token)

	return access_token
}

func getItems(access_token string) ItemArray {
	uri := "https://launcher-public-service-prod06.ol.epicgames.com/launcher/api/public/assets/Windows"
	req := RequestObj{
		uri:            uri,
		request_method: "GET",
		bearer:         "bearer " + access_token,
	}
	resp := Request(req)

	var result ItemArray

	json.NewDecoder(resp).Decode(&result.Collection)

	//log.Println(result.Collection)
	return result
}

func getItemInfo(access_token string, item_id string, app_name string) Item {
	req := RequestObj{
		uri:            "https://launcher-public-service-prod06.ol.epicgames.com/launcher/api/public/assets/Windows/" + item_id + "/" + app_name + "?label=Live",
		request_method: "GET",
		bearer:         "bearer " + access_token,
	}

	resp := Request(req)
	var result Item
	json.NewDecoder(resp).Decode(&result)
	//log.Println(result)

	return result
}

func getItemChunkManifest(access_token string, uri string) ChunkManifest {
	req := RequestObj{
		uri:        uri,
		bearer:     "bearer " + access_token,
		user_agent: "game=UELauncher, engine=UE4, build=revelations",
	}

	resp := Request(req)
	var result ChunkManifest
	json.NewDecoder(resp).Decode(&result)
	//log.Println(result)
	return result
}

func getDownloadList(manifest ChunkManifest) []string {
	log.Println(manifest)
	base_url := "https://download.epicgames.com/Builds/Rocket/Automated/" + manifest.AppNameString + "/CloudDir/ChunksV3/"
	list := []string{}
	count := 0
	for chunk, element := range manifest.ChunkHashList {
		count++
		group_int, err := strconv.Atoi(manifest.DataGroupList[chunk])
		if err != nil {
			log.Fatalln(err)
		}
		group := fmt.Sprintf("%02d", group_int)

		hash := chunkHashToReverseHexEncoding(element, 8)
		//log.Println(manifest.DataGroupList[chunk])
		//log.Println(group)

		download_url := base_url + group + "/" + hash + "_" + chunk + ".chunk"
		//if(hash == "3008B35C96D41603") {
			list = append(list, download_url)
		//}
	}

	return list
}

func chunkHashToReverseHexEncoding(chunk_hash string, length int) string {
	out_hex := ""
	runes := []rune(chunk_hash)
	for i := 0; i < length; i++ {
		start := i * 3
		end := start + 3
		substr := string(runes[start:end])
		//log.Println(runes[start:end], runes)
		if substr == "000" {
			substr = "0"
		}
		b, err := strconv.Atoi(substr)
		if err != nil {
			log.Fatalln(err)
		}
		out_hex = strings.ToUpper(hex.EncodeToString([]byte{byte(b)})) + out_hex
	}

	//log.Println(out_hex)

	return out_hex
}

func downloadFile(filepath string, url string) {
	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		log.Fatalln(err)
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	// Writer the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
}

type FileUrl struct {
	Uri string
	Name string
	idx int
}

var urls = make(chan FileUrl, 0)

func workerDownload() {
		for {
			select {
			case url := <- urls:
				downloadFile(url.Name, url.Uri)
				log.Println(url.idx, "out of", total)
			}
		}
}

var total = 0;

func downloadChunkFiles(download_urls []string) {
	total = len(download_urls)
	for k, uri := range download_urls {
		//log.Println(uri)
		parts := strings.Split(uri, "/")
		filename := parts[len(parts)-1]
		//downloadFile("./downloads/"+filename, uri)
		urls <- FileUrl{Uri:uri, Name:"./downloads/" + filename, idx : k}
	}

	log.Println("Done Generating list, let workers do their thing")
	time.Sleep(time.Second * 10)
}

func scanDir(dir string) []string {
	files := []string{}

	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		files = append(files, path)
		return nil
	})
	if err != nil {
		log.Fatalln(err)
	}

	return files
}

func extractAssets(manifest ChunkManifest) {
	// Scan the downloads directory for files
	files := scanDir("./downloads")
	for _, path := range files {
		//log.Println(path)

		if path != "./downloads" {
			file, err := os.Open("./" + path)
			if err != nil {
				log.Println("Top Dead")
				log.Fatalln(err)
			}

			header := make([]byte, 41)
			count, err := file.Read(header)
			if err != nil {
				log.Println(path)
				log.Println("Head Dead")

				exec.Command("rm ./" + path)
				log.Println("Deleted File")
				continue
			}

			stats, err := file.Stat();
			if err != nil {
				log.Println("Stat Dead")
				log.Fatalln(err)
			}

			header_size := header[8]
			compressed := header[40]
			log.Println("Count", count)


			chunk := make([]byte, stats.Size() - int64(header_size))
			file.Seek(int64(header_size), 0)
			log.Println("Seek File")
			chunk_count, err := file.Read(chunk)
			if err != nil {
				log.Println("Seek Dead")
				log.Fatalln(err)
			}


			log.Println("Compressed?", compressed)
			if compressed == 1 {
				log.Println(path)
				b := bytes.NewReader(chunk[0 : chunk_count])
				z, err := zlib.NewReader(b)
				if err != nil {
					log.Fatalln(err)
				}

				defer z.Close()

				p, err := ioutil.ReadAll(z)
				if err != nil {
					log.Println("Dead3")
				}
				log.Println("Not Dead")


				nameSplit := strings.Split(path, "/")
				fullName := nameSplit[len(nameSplit) - 1]
				name := strings.Split(fullName, "_")[1]
				log.Println(name)

				if(name == "F9C5411E414B4C0E3916FD83BC3BAF7C.chunk") {
					log.Println("Here123");
					os.Exit(1)
				}

				file, err = os.OpenFile("./renamed/" + name, os.O_RDONLY|os.O_CREATE|os.O_WRONLY, 0666)


				if err != nil {
					log.Fatalln(err)
				}

				file.Write(p)
			} else {
				nameSplit := strings.Split(path, "/")
				fullName := nameSplit[len(nameSplit) - 1]
				name := strings.Split(fullName, "_")[1]

				file, err = os.OpenFile("./renamed/" + name, os.O_RDONLY|os.O_CREATE|os.O_WRONLY, 0666)

				if err != nil {
					log.Println("Dead")
					log.Fatalln(err)
				}
				log.Println("Not Dead uncompressed")

				file.Write(chunk)

				log.Println("MAde it")
			}

			//defer file.Close()

		}

	}

	log.Println("After loop");

	for _, element := range manifest.FileManifestList {
		//fileSize := 0;
		//fileName := "./extracted/" + element.Filename
		fileSplit := strings.Split(element.Filename, "/")
		fileDir := strings.Join(fileSplit[0:len(fileSplit) - 1], "/")
		os.MkdirAll(fileDir, 0777);

		log.Println("Loop", element)

		topfile, err := os.OpenFile(element.Filename, os.O_RDONLY|os.O_CREATE|os.O_WRONLY, 0777)

		if err != nil {
			log.Fatalln(err)
		}

		// Determine FileSize
		fileSize := 0
		for _, chunk := range element.FileChunkParts {
			chunkSizeString, err :=  hex.DecodeString(chunkHashToReverseHexEncoding(chunk.Size, 4))
			chunkSize := int64(binary.BigEndian.Uint32(chunkSizeString))
			if err != nil {
				log.Println("Dead2")
				log.Fatalln(err)
			}
			fileSize += chunkSize
		}

		for _, chunk := range element.FileChunkParts {

			log.Println(chunk)

			chunkGuid := chunk.Guid
			//log.Println(chunk.Guid)
			chunkOffsetString, err :=  hex.DecodeString(chunkHashToReverseHexEncoding(chunk.Offset, 4))
			chunkSizeString, err :=  hex.DecodeString(chunkHashToReverseHexEncoding(chunk.Size, 4))
			log.Println("OffsetString", chunkOffsetString, chunkHashToReverseHexEncoding(chunk.Size, 4), chunkSizeString, chunk.Offset, chunk.Size)
			if err != nil {
				log.Println("Dead2")
				log.Fatalln(err)
			}

			log.Println("Not Dead 2")

			chunkOffset := int64(binary.BigEndian.Uint32(chunkOffsetString))
			chunkSize := int64(binary.BigEndian.Uint32(chunkSizeString))

			log.Println("Offset and Size", chunkOffset, chunkSize)


			buffer := make([]byte, chunkSize)

			file, err := os.Open("./renamed/" + chunkGuid + ".chunk")
			if err != nil {
				log.Fatalln(err)
			} else {
				//file.Seek(chunkOffset, 0);
				file.Read(buffer)

				log.Println("Wrote to " + element.Filename, buffer)
				topfile.Write(buffer)
				//os.Exit(1);
			}
		}

		defer topfile.Close();
		log.Println(element.FileChunkParts)
	}

}

func main() {
	godotenv.Load()

	for i := 0; i < 17; i++ {
		go workerDownload()
	}

	access_token := oauthLogin()
	items := getItems(access_token)

	for _, element := range items.Collection {
		item := getItemInfo(access_token, element.CatalogItemId, element.AppName)

		if(item.AppName == "KenneySpritePack") {
			download_url := item.Info.Manifest.Distribution + item.Info.Manifest.Path
			log.Println(download_url)
			manifest := getItemChunkManifest(access_token, download_url)
			//download_urls := getDownloadList(manifest)
			//log.Println(download_urls)
			//downloadChunkFiles(download_urls)

			extractAssets(manifest);
		}

		//time.Sleep(60);

	}

}
